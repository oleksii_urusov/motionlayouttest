package com.example.motionlayouttest

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce

private const val CLICK_TRANSITION_DURATION = 200

private const val MAX_CLICK_X_DELTA = 50
private const val MAX_CLICK_Y_DELTA = 50

private const val MAX_ALLOWED_SPRING_MOVE_PERCENT = 0.3F

class ClickSwipableMotionLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var startX: Float = -1F
    private var startY: Float = -1F

    private var dX: Float = 0F
    private var dY: Float = 0F

    private var isSpringMoving = false

    @IdRes
    private var currentState: Int = R.id.card_one_open
    private val cardOne: View
    private val cardTwo: View
    private val cardThree: View
    private val motionLayout: MotionLayout
    private val springAnimation: SpringAnimation

    private val maxAllowedSpringMove: Float
        get() = height * MAX_ALLOWED_SPRING_MOVE_PERCENT

    private val isInProgress
        get() = motionLayout.progress > 0.0f && motionLayout.progress < 1.0f

    private val MotionEvent.isClickableViewTouched
        get() = touchEventInsideTargetView(cardOne, this) ||
                touchEventInsideTargetView(cardTwo, this) ||
                touchEventInsideTargetView(cardThree, this)

    init {
        View.inflate(context, R.layout.click_swipable_layout, this)
        cardOne = findViewById(R.id.card_one)
        cardTwo = findViewById(R.id.card_two)
        cardThree = findViewById(R.id.card_three)
        motionLayout = findViewById(R.id.root_motion_layout)

        motionLayout.setTransitionListener(OnTransitionEndListener { _, newState ->
            this.currentState = newState
        })

        springAnimation = createSpringAnimation()
    }

    private fun createSpringAnimation() = SpringAnimation(motionLayout, SpringAnimation.Y, 0F).also {
        val spring = SpringForce(0f)
        spring.stiffness = SpringForce.STIFFNESS_MEDIUM
        spring.dampingRatio = SpringForce.DAMPING_RATIO_LOW_BOUNCY

        it.spring = spring
    }

    override fun onInterceptTouchEvent(event: MotionEvent) = if (this.isInProgress || event.isClickableViewTouched) {
        super.onInterceptTouchEvent(event)
    } else {
        true
    }

    private fun isFirstCardTouched(event: MotionEvent) = touchEventInsideTargetView(cardOne, event)
    private fun isSecondCardTouched(event: MotionEvent) = touchEventInsideTargetView(cardTwo, event)
    private fun isThirdCardTouched(event: MotionEvent) = touchEventInsideTargetView(cardThree, event)

    private fun touchEventInsideTargetView(v: View, ev: MotionEvent): Boolean {
        if (ev.x > v.left && ev.x < v.right) {
            if (ev.y > v.top && ev.y < v.bottom) {
                return true
            }
        }
        return false
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.isClickableViewTouched && !this.isInProgress) {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    startX = event.x
                    startY = event.y

                    dX = motionLayout.x - event.rawX
                    dY = motionLayout.y - event.rawY

                    springAnimation.cancel()
                }

                MotionEvent.ACTION_MOVE -> {
                    if (isMovementAllowed(event)) {
                        springMove(event)
                    } else {
                        stopStringMovement()
                    }
                }

                MotionEvent.ACTION_UP -> {
                    val endX = event.x
                    val endY = event.y
                    if (isAClick(startX, endX, startY, endY) && isClickAllowed()) {
                        if (doClickTransition(event)) {
                            return true
                        }
                    }
                    stopStringMovement()
                }
            }
        }

        return super.dispatchTouchEvent(event)
    }

    private fun springMove(event: MotionEvent) {
        isSpringMoving = true
        motionLayout.animate()
            .y(event.rawY + dY)
            .setDuration(0)
            .start()
    }

    private fun isMovementAllowed(event: MotionEvent) =
        !isInProgress &&
                isThirdCardTouched(event) &&
                event.y > startY &&
                event.rawY + dY < y + maxAllowedSpringMove

    private fun stopStringMovement() {
        isSpringMoving = false
        springAnimation.start()
    }

    private fun isClickAllowed() = motionLayout.currentState != R.id.card_one_open_collapsed

    private fun doClickTransition(event: MotionEvent): Boolean {
        val newState = when {
            isFirstCardTouched(event) -> R.id.card_one_open
            isSecondCardTouched(event) -> R.id.card_two_open_click
            isThirdCardTouched(event) -> R.id.card_three_open
            else -> R.id.card_one_open
        }

        if (currentState != newState) {
            motionLayout.setTransitionDuration(CLICK_TRANSITION_DURATION)
            motionLayout.transitionToState(newState)
        }

        return true
    }

    private fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
        val differenceX = Math.abs(startX - endX)
        val differenceY = Math.abs(startY - endY)
        return !(differenceX > MAX_CLICK_X_DELTA || differenceY > MAX_CLICK_Y_DELTA)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean = false
}
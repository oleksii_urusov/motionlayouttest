package com.example.motionlayouttest

import androidx.constraintlayout.motion.widget.MotionLayout

class OnTransitionEndListener(private val action: (layout: MotionLayout?, currentState: Int) -> Unit) :
    MotionLayout.TransitionListener {

    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
        // Do nothing
    }

    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
        // Do nothing
    }

    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
        // Do nothing
    }

    override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
        action(p0, p1)
    }
}